package hashSqlTest.readProperties;

import java.io.InputStream;
import java.util.Properties;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class PropertiesRead {
	private final String nameFile;

	public String extract(final String properties) {
		Properties prop = new Properties();
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream in = classLoader.getResourceAsStream(nameFile); 
		
			prop.load(in);
			log.info(""+prop.getProperty(properties));  
			in.close();
	
		} catch (Exception e) {
			log.error("error",e);
		}

		return prop.getProperty(properties);

	}

}
