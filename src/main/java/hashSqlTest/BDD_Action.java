package hashSqlTest;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import hashSqlTest.readProperties.PropertiesRead;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@AllArgsConstructor
public class BDD_Action {

	private final String bdd_url;
	private final String bdd_user;
	private final String bdd_password;

	public void init() {
		log.info("--- init s�quence ---");
		final String query = "SELECT VERSION()";
		// test si en ligne -> num de version
		try (Connection con = DriverManager.getConnection(bdd_url, bdd_user, bdd_password);
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery(query)) {

			if (rs.next()) {
				log.info(rs.getString(1));
			}

		} catch (SQLException ex) {
			log.error("error", ex);
		}

	}

	public void addUsers(final String user, final String password) {
		// TODO v�rifier si user unique
		log.info("--- create new user ---");
		try (Connection con = DriverManager.getConnection(bdd_url, bdd_user, bdd_password);
				Statement st = con.createStatement()) {
			//pepper
			PropertiesRead prop = new PropertiesRead("param.properties");
			final String pepper = prop.extract("pepper");
			byte[] pepperByte = pepper.getBytes();
			
			final String passwordToHash = password;
			log.info("password free : {}", passwordToHash);
			passwordToHash.concat(pepper);
			// generate salt
			SecureRandom random = SecureRandom.getInstance("DRBG");
			byte[] salt = new byte[16];
			random.nextBytes(salt);
			MessageDigest md = null;
			md = MessageDigest.getInstance("SHA-512");
			md.update(salt);
			md.update(pepperByte);
			byte[] hashedPassword = md.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));

			String mdpString = new String(hashedPassword);
			String saltString = new String(salt);
			log.info("password hash+salt : {}", mdpString);
			log.info("salt : {}", saltString);

			String sql = "INSERT INTO users (name_users,pasword_users,sel_users) VALUES(\"" + user + "\",\"" + mdpString
					+ "\",\"" + saltString + "\")";
			st.executeUpdate(sql);
			log.info("succces insert !");

		} catch (Exception ex) {
			log.error("error", ex);
		}

	}

	public void testUserExist(final String user, final String password) {
		log.info("--- find and verify password user ---");
		// found salt
		final String queryFoundSalt = "SELECT sel_users FROM users WHERE name_users='" + user + "';";
		byte[] saltToTest = null;
		try (Connection con = DriverManager.getConnection(bdd_url, bdd_user, bdd_password);
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery(queryFoundSalt)) {

			if (rs.next()) {
				log.info("salt is found : {}", rs.getString(1));
				saltToTest = rs.getString(1).getBytes();
			} else {
				log.warn("salt or user not fond");
			}
		} catch (SQLException ex) {
			log.error("error", ex);
		}

		// found password with salt
		//pepper
		PropertiesRead prop = new PropertiesRead("param.properties");
		final String pepper = prop.extract("pepper");
		byte[] pepperByte = pepper.getBytes();
		log.info("pepper is : {}",pepper);
		
		final String passwordToHash = password;
		passwordToHash.concat(pepper);
		byte[] salt = saltToTest;
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-512"); 
		} catch (Exception e) {
			System.out.print(e);
		}
		md.update(salt);
		md.update(pepperByte);
		byte[] hashedPassword = md.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));
		String passwordHashedString = new String(hashedPassword);
		log.info("test with hash : {}", passwordHashedString);
		
		final String queryVerif = "SELECT name_users FROM users WHERE name_users= ? AND pasword_users= ? ;";
	    
		try (Connection con = DriverManager.getConnection(bdd_url, bdd_user, bdd_password);
				PreparedStatement userVerif = con.prepareStatement(queryVerif);	) {
		
			userVerif.setString(1, user);
			userVerif.setString(2, passwordHashedString);

			ResultSet rs = userVerif.executeQuery();

			if (rs.next()) {
				log.info("succes ! with user : {}", rs.getString(1));
			} else {
				log.warn("user with this password not found");
			}

		} catch (Exception ex) {
			log.error("error", ex);
		}
	}

}
